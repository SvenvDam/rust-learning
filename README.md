## Rust CS50

Implementation of some exercises I did during the CS50 course in Rust.

```bash
cargo run <program name> [additional params]
```