use std::collections::HashMap;

pub fn hello_world() {
    println!("Hello, world")
}

pub fn mario(height: usize) {
    let spacer = "  ";
    for i in 0..height {
        let lead_space = " ".repeat(height - i);
        let block = "#".repeat(i);
        println!("{}{}{}{}", lead_space, block, spacer, block);
    }
}

pub fn water(minutes: usize) {
    println!("{} bottles", 12 * minutes);
}

pub fn descriptive_stats(l: Vec<i32>) {
    let len = l.len();
    let index = (len as f32 / 2.0).ceil() as usize - 1;

    let mut sum = 0;
    let mut counts: HashMap<_, _> = HashMap::new();
    let mut mode = 0;
    let mut max_cnt = 0;

    for i in &l {
        sum += i;
        let cnt = counts.entry(i).or_insert(0);
        *cnt += 1;
    }

    for (k, v) in counts {
        if v > max_cnt {
            mode = *k;
            max_cnt = v;
        }
    }

    let mean = sum as f32 / len as f32;
    let median = l[index];

    println!("Mean: {}", mean);
    println!("Median: {}", median);
    println!("Mode: {}", mode);
}

pub fn pig_latin(str: String) {
    let first = &str.chars().nth(0).expect("Empty string passed!");

    let res = match &first {
        'a' | 'e' | 'o' | 'u' | 'i' => str + "-hay",
        _ => format!("{}-{}ay", &str[1..], &str[0..1])
    };

    println!("{}", res)
}

pub fn employees() {
    let mut cache: HashMap<String, Vec<String>> = HashMap::new();
    loop {
        let mut command = String::new();
        std::io::stdin().read_line(&mut command).expect("Could not read");

        let words: Vec<&str> = command
            .split_whitespace()
            .collect();

        let operation = words
            .get(0)
            .expect("Empty command!");

         match operation.as_ref() {
            "exit" => std::process::exit(0),
            "add" => match (words.get(1), words.get(3)) {
                (Some(name), Some(dep)) => {
                    let names = cache
                        .entry(String::from(*dep))
                        .or_insert(Vec::new());
                    names.push(String::from(*name))
                },
                _ => println!("Cannot add employee!")
            },
            "get" => {
                let dep = words.get(1).expect("No departement passed!");
                match cache.get(*dep) {
                    Some(vec) => {
                        for name in vec {
                            println!("\t{}", name)
                        }
                    },
                    None => println!("Department not found!")
                }
            },
            _ => println!("Command not recognized!")
        }
        println!(" ")
    }
}