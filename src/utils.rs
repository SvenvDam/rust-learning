use std::env;

pub fn get_arg(i: usize) -> String {
    return env::args()
        .collect::<Vec<String>>()
        .get(i)
        .expect(format!("No argument found at place {}", i).as_ref())
        .to_string();
}