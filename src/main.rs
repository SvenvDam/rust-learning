mod programs;
mod utils;

use programs as p;

fn main() {
    let program = utils::get_arg(1);

    match &program[..] {
        "hello_world" => p::hello_world(),

        "mario" => p::mario(
            utils::get_arg(2)
                .parse::<usize>()
                .unwrap()),

        "water" => p::water(
            utils::get_arg(2)
                .parse::<usize>()
                .unwrap()),

        "descr_stats" => p::descriptive_stats(vec![1, 1, 2]),

        "pig_latin" => p::pig_latin(
            utils::get_arg(2)
        ),
        "employees" => p::employees(),

        _ => println!("No valid program passed")
    }
}